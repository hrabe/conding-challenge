﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("Lake-Barbacue-Test")]

namespace Lake_Barbacue
{
    internal class Point {
        public double X;
        public double Y;
    }
    internal class Geometry
    {
        internal const double E = 1e-3;

        internal static double ComputeDiscriminant(double A, double B, double C){
            return B*B - 4*A*C;
        }

        internal static IEnumerable<Point> GetIntersectionX(Point boat, double aP, double bP, double R){
            double aa = bP;
            double bb = -aP;

            double a = (bb*bb)/(aa*aa) + 1;
            double b = 2*boat.X*bb/aa - 2*boat.Y;
            double c = boat.Y*boat.Y + boat.X*boat.X - R*R;

            // ax + by + c = 0 => ax + by = 0 => x = -b/ay
            // (x-x0)^2 + (y-y0)^2 = R^2
            // (x+b/a*y0)^2 + (y-y0)^2 = R^2 => x^2 + 2*x*b/a*y0 + (b/a)^2y0^2 + y^2 -2*y*y0 + y0^2 = R^2

            // double discriminant = (2*x*b/a -2*y)*(2*x*b/a -2*y) - 4 *((b/a)*(b/a) + 1)*(x*x - R*R);
            double discriminant = ComputeDiscriminant(a, b, c);

            double x1 = 0;
            double x2 = 0;
            double y1 = 0;
            double y2 = 0;

            if(Math.Abs(discriminant) < 1e-12){
                y1 = (-b)/(2*a);
                x1 = -(bb/aa)*y1;
                return new List<Point> { new Point {X=x1, Y=y1} };
            }
            else if (discriminant < 0){
                // no solution
                return null;
            }
            else{
                // two solutions
                y1 = (-b + Math.Sqrt(discriminant))/(2*a);
                y2 = (-b - Math.Sqrt(discriminant))/(2*a);
                x1 = -(bb/aa)*y1;
                x2 = -(bb/aa)*y2;
                return new List<Point> { 
                    new Point {X=x1, Y=y1},
                    new Point {X=x2, Y=y2}
                };
            }
        }

        internal static List<Point> GetIntersectionY(Point boat, double R){
            // y0 = 0 
            // (x-x0)^2 + (y)^2 = R^2
            double discriminant = ComputeDiscriminant(1, -2*boat.X, boat.X*boat.X + boat.Y*boat.Y - R*R);

            double x1 = 0;
            double x2 = 0;
            double y1 = 0;
            double y2 = 0;

            if(Math.Abs(discriminant) < 1e-12){
                x1 = boat.X;
                return new List<Point> { new Point {X=x1, Y=y1} };
            }
            else if (discriminant < 0){
                // no solution
                return null;
            }
            else{
                // two solutions
                x1 = (2*boat.X + Math.Sqrt(discriminant))/2;
                x2 = (2*boat.X - Math.Sqrt(discriminant))/2;
                return new List<Point> { 
                    new Point {X=x1, Y=y1},
                    new Point {X=x2, Y=y2}
                };
            }
        }

        internal static double PointsDistance(Point A, Point B){
            return Math.Sqrt((A.X - B.X)*(A.X - B.X) + (A.Y - B.Y)*(A.Y - B.Y));
        }

        internal static bool IsInsideCircle(Point point, Point circle, double R){
            double dist = PointsDistance(point, circle);
            return dist < R + E;
        }

        internal static Point[] GetPointsInsideRadiusGivenLineAB(Point[] points, Point A, Point B, long R){
            return points.Where(p => MinDistance(A, B, p) < R + E).ToArray();
        }

        // Function to find distance// Function to return the minimum distance
        // between a line segment AB and a point E
        internal static double MinDistance(Point A, Point B, Point E)
        {
            // vector AB
            Point AB = new Point();
            AB.X = B.X - A.X;
            AB.Y = B.Y - A.Y;
        
            // vector BP
            Point BE = new Point();
            BE.X = E.X - B.X;
            BE.Y = E.Y - B.Y;
        
            // vector AP
            Point AE = new Point();
            AE.X = E.X - A.X;
            AE.Y = E.Y - A.Y;
        
            // Variables to store dot product
            double AB_BE, AB_AE;
        
            // Calculating the dot product
            AB_BE = (AB.X * BE.X + AB.Y * BE.Y);
            AB_AE = (AB.X * AE.X + AB.Y * AE.Y);
        
            // Minimum distance from
            // point E to the line segment
            double reqAns = 0;
        
            // Case 1
            if (AB_BE > 0)
            {
                // Finding the magnitude
                double y = E.Y - B.Y;
                double x = E.X - B.X;
                reqAns = Math.Sqrt(x * x + y * y);
            }
        
            // Case 2
            else if (AB_AE < 0)
            {
                double y = E.Y - A.Y;
                double x = E.X - A.X;
                reqAns = Math.Sqrt(x * x + y * y);
            }
        
            // Case 3
            else
            {
                // Finding the perpendicular distance
                double x1 = AB.X;
                double y1 = AB.Y;
                double x2 = AE.X;
                double y2 = AE.Y;
                double mod = Math.Sqrt(x1 * x1 + y1 * y1);
                reqAns = Math.Abs(x1 * y2 - y1 * x2) / mod;
            }
            return reqAns;
        }

        /// <summary>
        /// Rotates one point around another
        /// </summary>
        /// <param name="pointToRotate">The point to rotate.</param>
        /// <param name="centerPoint">The center point of rotation.</param>
        /// <param name="angleInDegrees">The rotation angle in degrees.</param>
        /// <returns>Rotated point</returns>
        internal static Point RotatePoint(Point pointToRotate, Point centerPoint, double angleInRadians)
        {
            // double angleInRadians = angleInDegrees * (Math.PI / 180);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new Point
            {
                X = 
                    (cosTheta * (pointToRotate.X - centerPoint.X) -
                    sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y = 
                    (sinTheta * (pointToRotate.X - centerPoint.X) +
                    cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
        }
    }
}
