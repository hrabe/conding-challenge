using System;
using Xunit;
using Lake_Barbacue;

namespace Lake_Barbacue_Test
{
    public class GeometryTest
    {
        [Fact]
        public void ComputeDiscriminantTest()
        {
            double a = 1;
            double b = 3;
            double c = 1;

            double result = Geometry.ComputeDiscriminant(a,b,c);

            Assert.True(Math.Abs(result - 5) < 1e-12);
        }

        [Theory]
        [InlineData(0,0,1,0,2,0,1)]
        [InlineData(0,0,1,1,0,1, 0.707106781186547524400)] // Math.Sqrt(2)/2
        [InlineData(-1000000,-1000000,1000000,1000000,0,0,0)] // Math.Sqrt(2)/2
        public void MinDistanceTest(double aX, double aY,double bX, double bY,double cX, double cY, double expected){
            Point A = new Point{X=aX, Y=aY}; 
            Point B = new Point{X=bX, Y=bY}; 
            Point C = new Point{X=cX, Y=cY}; 

            double result = Geometry.MinDistance(A,B,C);
            Assert.True(Math.Abs(result - expected) < 1e-12);
        }
    }
}
