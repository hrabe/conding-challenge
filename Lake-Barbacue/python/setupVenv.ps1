$oldLoc = Get-Location

Set-Location $PSScriptRoot

if(Test-Path test-env){
    python -m venv test-env
}
.\test-env\Scripts\activate
python -m pip install -r requirements.txt

Set-Location $oldLoc