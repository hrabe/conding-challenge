import matplotlib.pyplot as plt
import os

# Draw multiple points.
def draw_multiple_points(points_x, points_y, circle_x, circle_y, circle_r, a, b):

    fig, ax = plt.subplots()
    ax.scatter(circle_x, circle_y, s=3, color='black')
    cir = plt.Circle((circle_x, circle_y), circle_r, color='r',fill=False)
    ax.plot([0, a], [0, b], color='g', linewidth=0.5)
    ax.scatter(points_x, points_y, s=8, color='blue')
    
    ax.add_patch(cir)
    ax.set_aspect('equal', adjustable='datalim')
    # Set chart title.
    plt.title("The Lake Barbacue")
    # Set x, y label text.
    plt.xlabel("x coordinate")
    plt.ylabel("y coordinate")
    plt.show()
if __name__ == '__main__':
    current_working_dir = os.path.dirname(os.path.realpath(__file__))
    print(current_working_dir)

    points_pathname = os.path.join(current_working_dir,  './points.txt')
    circle_pathname = os.path.join(current_working_dir,  './circle.txt')
    points_x = []
    points_y = []

    circle_x = 0
    circle_y = 0
    circle_r = 0
    
    count = 0
    with open(points_pathname, "r") as my_file:
        for line in my_file:
            str = line.split()
            if count == 0:
                a = (int)(str[0])
                b = (int)(str[1])
            else:
                points_x.append((float)(str[0]))
                points_y.append((float)(str[1]))
            count = count + 1
    with open(circle_pathname, "r") as my_file:
        for line in my_file:
            str = line.split()
            circle_x = (float)(str[0])
            circle_y = (float)(str[1])
            circle_r = (float)(str[2])
    
    
    draw_multiple_points(points_x, points_y, circle_x, circle_y, circle_r, a, b)
